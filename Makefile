al:
	gcc -fopenmp -o heat3D heat3D.c
	gcc -fopenmp -o heat3D_serial heat3D_serial.c

parallel:
	gcc -fopenmp -o heat3D heat3D.c

serial:
	gcc -fopenmp -o heat3D_serial heat3D_serial.c

clean:
	rm -f heat3D heat3D_serial
